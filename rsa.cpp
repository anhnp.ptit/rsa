#include <iostream>
#include <string>
#include <climits>
#include <math.h>
#include "BigInt.hpp"

using namespace std;
BigInt p, q, n, phi, d;
unsigned long long e;
string data;
vector<string> encode_data;
string decode_data;


BigInt getModInverse(BigInt e, BigInt phiN){
    BigInt x1 = 0, x2 = 1;
    BigInt y1 = 1, y2 = 0;
    while (phiN > 0){
        BigInt q = e/phiN;
        BigInt d = x2 - q * x1;
        BigInt k = y2 - q * y2;
        BigInt r = e - q * phiN;
        e = phiN;
        phiN = r;

        x2 = x1;
        x1 = d;
        y2 = y1;
        y1 = k;

    }
    return x2;
}

BigInt powAndMod(BigInt m, unsigned long long e, BigInt n){
    BigInt a = m;
    BigInt c = 1;
    if (e & 1 == 1){
        c = m;
    }
    e >>= 1;

    while (e != 0){
        a = (a * a) % n;
        if (e & 1 == 1){
            c = (a * c) % n;
        }
        e >>= 1;
    }
    return c;
}


unsigned long long num_random(unsigned long long minimum, unsigned long long maximum)
{
  unsigned long long result;
  do result = rand();
  while (result < minimum || maximum < result);
  return result;
}

bool isPrime(unsigned long long n)
{
    if (n <= 1)
        return false;

    for (unsigned long long i = 2; i < n; i++)
        if (n % i == 0)
            return false;

    return true;
}

bool isPrimeBig(BigInt number)
{
    for(BigInt i(2); i < sqrt(number); ++i)
        if (number % i == 0)
            return false;
    return true;
}

vector<string> encode(string data, vector<string> encode_data){
    for(char& c : data){
        encode_data.push_back(powAndMod(int(c), e, n).to_string());
    }
    return encode_data;
}

string decode(vector<string> encode_data, string decode_data){
    for(auto i = encode_data.begin(); i != encode_data.end(); ++i){
        decode_data += char(powAndMod(*i, d.to_long_long(), n).to_int());
    }
    return decode_data;
}

int main() {
    p = 937717931;
    q = 649733033;
    n = 609266315407114723;
    e = 718499077;
    d = 96960628728584573;
    data = "Ban Ma";
    encode_data = encode(data, encode_data);
    decode_data = decode(encode_data, decode_data);
    cout << "Ma hoa" << endl;
    for(auto i = encode_data.begin(); i != encode_data.end(); ++i){
        cout << *i << endl;
        }
    cout << "Giai ma: " << decode_data <<endl;
    return 0;
}

